# Use the cargo-lambda image for building
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

# Create a directory for your application
WORKDIR /usr/src/app

# Copy your source code into the container
COPY . .

# Build the Lambda function using cargo-lambda
RUN cargo lambda build --release --arm64

# Use a new stage for the final image
FROM public.ecr.aws/lambda/provided:al2-arm64

# Set the working directory for the lambda function
WORKDIR /mini10

# Copy the bootstrap binary from the builder stage
COPY --from=builder /usr/src/app/target/lambda/llm/bootstrap ./

# Copy the llama model here 
COPY --from=builder /usr/src/app/pythia-1b-q4_0-ggjt.bin ./

# Set the entrypoint for the Lambda function
ENTRYPOINT ["/mini10/bootstrap"]
