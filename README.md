# Mini10 - Rust Serverless Transformer Endpoint
This project aims to develop and deploy a LLM to a serverless AWS Lambda function, using Rust and Cargo Lambda, and to set up a local LLM inference endpoint.

## Steps
### Step 0: LLM Model 
Among the 11 LLM models listed in the `Hugging Face: rustformers - Large Language Models in Rust` repo, `rustformers/pythia-ggml` is chosen since it is a relatively small LLM and therefore more suitable with AWS Lambda function and the practice of this project.

### Step 1: Setup
1. Initializing new AWS Lambda project in Rust using command line `cargo lambda new <PROJECT_NAME>` in terminal.
2. Add necessary dependencies to `Cargo.toml` file.
3. Add functional implementations and inference endpoint in `main.rs` file.
4. Run `cargo lambda watch`, and to test locally 
- screenshot of the local test
![localtest](localtest.png)

### Step 2: Dockerization and Elastic Container Registry (ECR)
1. Then we can navigate into the `Identity and Access Management (IAM)` under AWS console. 
2. Add attach necessary polices including 
* `IAMFullAccess`
* `AWSLambda_FullAccess`
* `AWSAppRunnerServicePolicyForECRAccess`
* `EC2InstanceProfileForImageBuilderECRContainerBuilds`
3. Naviagte into `ECR` under AWS console and create a new repository.
4. Associates our local Docker with AWS through
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com

```
5. Then, build the docker image by
```
docker buildx build --progress=plain --platform linux/arm64 -t transformer .
```
- screenshot of the docker build
![docker_build](build.png)

6. Then, tag the image by
```
docker tag transformer:latest <aws_id>.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
```
7. Finally, push the image to the ECR repository we just created by
```
docker push <aws_id>.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
```

### Step 3: AWS Lambda
1. After sucessfuly pushing the docker image to ECR, navigate into `Lambda` under AWS console. 
2. Create a new function with the option `Container image`, enter the Amazon ECR image URL, and choose `arm64` architecture.
3. Then, click into your lambda function, and under `configuration`, navigate into the `General configuration`, and adjust the Memory and timeout setting based on the characteristics of your LLM (token etc.).
4. Then, click into the `functional URL`, create a new function URL with `CORS enabled`.
- screenshot of the AWS Lambda function
![lambda](lambda.png)
- screenshot of the monitoring
![monitoring](monitor.png)



